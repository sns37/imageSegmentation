#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <math.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <cstdlib>
#include <ctime>

using namespace std;
using namespace cv;

/* Constants used in the energy eqn*/
const double beta = 0.5*2;
const double var = 342.25;
const double wt_clq_n = 0.05, wt_clq_e = 0.05, wt_clq_ne = 0.05, wt_clq_se = 0.05, wt_clq_top = 0.05, wt_clq_right = 0.05;
const unsigned char labels[5] = {0, 63, 127, 191, 255};
const int max_iter = 5000;

/* Annealing Parameters */
const float T = 0.1;

/* Image Data */
int IMG_H, IMG_W;
char* ilabel_file_name = "resources/pixel_values_org.csv";
char* output_file_name = "resources/pixel_values_org.csv";
int*** singleton_E;

/* Time values */
double tstart, tend;

void preCalSingletonEnergy(Mat input_img){

    //initialize Singletons
    singleton_E = new int**[IMG_H];
    for(int i = 0; i < IMG_H; ++i){
        singleton_E[i] = new int*[IMG_W];
        for(int j = 0 ; j < IMG_W; ++j)
            singleton_E[i][j] = new int[5];
    }

    for(int i = 0 ; i < IMG_H; i++){
        for(int j = 0; j < IMG_W; j++){
            for(int iter_label = 0; iter_label < 5; iter_label++){
                unsigned char cur_label = labels[iter_label];

                singleton_E[i][j][iter_label] = 0;

                singleton_E[i][j][iter_label] += wt_clq_top * abs(cur_label - input_img.at<uchar>(i, j - 2));
                singleton_E[i][j][iter_label] += wt_clq_n * abs(cur_label - input_img.at<uchar>(i, j - 1));
                singleton_E[i][j][iter_label] += wt_clq_ne * abs(cur_label - input_img.at<uchar>(i + 1, j - 1));
                singleton_E[i][j][iter_label] += wt_clq_e * abs(cur_label - input_img.at<uchar>(i + 1, j));
                singleton_E[i][j][iter_label] += wt_clq_right * abs(cur_label - input_img.at<uchar>(i + 2, j));
                singleton_E[i][j][iter_label] += wt_clq_se * abs(cur_label - input_img.at<uchar>(i + 1, j + 1));

                singleton_E[i][j][iter_label] /= (2 * var);
            }
        }
    }
}

Mat initLabelImg(Mat input_img, Mat label_input_img)
{
	for(int i = 0; i < IMG_H; i++){
		for(int j = 0 ; j < IMG_W; j++){
			if(input_img.at<uchar>(i, j) < 223)
				label_input_img.at<uchar>(i, j) = 191;

			if(input_img.at<uchar>(i, j) < 159)
				label_input_img.at<uchar>(i, j) = 127;

			if(input_img.at<uchar>(i, j) < 95)
				label_input_img.at<uchar>(i, j) = 63;

			if(input_img.at<uchar>(i, j) < 31)
				label_input_img.at<uchar>(i, j) = 0;
		}
	}
	preCalSingletonEnergy(input_img);
	
	return label_input_img;
}

void writeToCSV(char* path, Mat label)
{
		/* Write the pixel values to a csv file */
		FILE* fp = fopen(path, "w+");
		for (int i = 0; i < IMG_H; i++) {
			for (int j = 0; j < IMG_W; j++) {
				fprintf(fp, "%d,", label.at<uchar>(i, j));
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
}

Mat greyToRGB(Mat input)
{
	Mat output(IMG_W, IMG_H, CV_8UC3); 
	unsigned char label = 0;

	for(int x = 0; x < IMG_H; x++){
		for(int y = 0; y < IMG_W; y++){
			label = input.at<uchar>(x, y);

			switch(label){
				/* RED Colored Pixel */
				case 0:
					output.at<Vec3b>(x, y)[0] = 255;
					output.at<Vec3b>(x, y)[1] = 0;
					output.at<Vec3b>(x, y)[2] = 0;
				break;

				/* GREEN Colored Pixel */
				case 63:
					output.at<Vec3b>(x, y)[0] = 124;
					output.at<Vec3b>(x, y)[1] = 252;
					output.at<Vec3b>(x, y)[2] = 0;
				break;

				/* BLUE Coloured Pixel */
				case 127:
				output.at<Vec3b>(x, y)[0] = 65;
					output.at<Vec3b>(x, y)[1] = 105;
					output.at<Vec3b>(x, y)[2] = 255;
				break;

				/* YELLOW Coloured Pixel */
				case 191:
					output.at<Vec3b>(x, y)[0] = 255;
					output.at<Vec3b>(x, y)[1] = 215;
					output.at<Vec3b>(x, y)[2] = 0;
				break;

				/* MILD BROWN Coloured Pixel */
				case 255:
					output.at<Vec3b>(x, y)[0] = 0;
					output.at<Vec3b>(x, y)[1] = 0;
					output.at<Vec3b>(x, y)[2] = 0;
				break;
			}
		}
	}

	return output;
}

int main(int argc, char** argv)
{
	if(argc != 2){
		printf("\nPlease input image as an argument\n");
		return 0;
	}

	string img_path = argv[1];
	/* See random number generator using random device */
	random_device rd;
	mt19937 gen(rd());

	if(!img_path.empty()) {
		//Load Image from cmd arg
		Mat input_img = imread(img_path, CV_LOAD_IMAGE_GRAYSCALE);

		//Check if the img is loaded properly
		if(input_img.empty()) {
			CV_Error(CV_StsBadArg, "Sample image is empty. Please adjust your path, so it points to a valid input image!");
		}
		//Print image dimensions
		printf("Starting.... \nImage Loaded: Height: %d, Width %d.\n", input_img.rows, input_img.cols);
		IMG_H = input_img.rows, IMG_W = input_img.cols;
		
		//Create a new 2D Mat for grey scale and set its pixels to 255
		Mat label_input_img(IMG_W, IMG_H, CV_8UC1); 
		/* start measuring the comutation time */
		tstart = omp_get_wtime();
		label_input_img.setTo(Scalar(255));		
		//writeToCSV(ilabel_file_name, label_input_img);
		label_input_img = initLabelImg(input_img, label_input_img);

		for(int i = 0; i < max_iter; i++){
			
			{			
				/* Process the black boxes of stencil on checker board */
				#pragma omp for
				for(int black_i = 0; black_i < IMG_H; black_i++){
					for(int black_j = 0; black_j < (IMG_W/2); black_j++){
						int x = black_i, y = (2 * black_j) + (black_i % 2);

						if(x > -1 && y > -1 && x < (label_input_img.rows) && y < (label_input_img.cols)){
							/* Calculate the Local Energy(LE) for 5 labels given energy of neighbours */
							/* Use Gibbs distribution to obtain probability of taking a given label */
							vector<double> p_weights;

							for(int iter_label = 0; iter_label < 5; iter_label++){
								unsigned char cur_label = labels[iter_label];

								/* Calculate energy of singleton */
								double LE = singleton_E[x][y][iter_label];						

								/* Caluculate energy of doubleton = diff(Pixel & Neighbours) */
								if(x - 1 > -1)
									LE += beta * (label_input_img.at<uchar>(x - 1, y) != cur_label);
								if(x + 1 < IMG_H)
									LE += beta * (label_input_img.at<uchar>(x + 1, y) != cur_label);
								if(y - 1 > -1)
									LE += beta * (label_input_img.at<uchar>(x, y - 1) != cur_label);
								if(y + 1 < IMG_W)
									LE += beta * (label_input_img.at<uchar>(x, y + 1) != cur_label);
								
								/* Assign probability to each label this is used as weight */
								p_weights.push_back(expf(-(1 / T) * LE));
							}
							discrete_distribution<> weight(p_weights.begin(), p_weights.end());
							int rand_index = weight(gen);
							label_input_img.at<uchar>(x, y) = labels[rand_index];
						}
					}
				}

				/* Process the white boxes of stencil on checker board */
				for(int white_i = 0; white_i < IMG_H; white_i++){
					for(int white_j = 0; white_j < (IMG_W/2); white_j++){
						int x = white_i, y = (2 * white_j) + ((white_i + 1) % 2);

						if(x > -1 && y > -1 && x < label_input_img.rows && y < label_input_img.cols){

	                    /* Calculate the Local Energy(LE) for 5 labels given energy of neighbours */
	                    /* Use Gibbs distribution to obtain probability of taking a given label */
							vector<double> p_weights;

							for(int iter_label = 0; iter_label < 5; iter_label++){
								unsigned char cur_label = labels[iter_label];

	                            /* Calculate energy of singleton */
								double LE = singleton_E[x][y][iter_label];

	                        	/* Caluculate energy of doubleton = diff(Pixel & Neighbours) */
								if(x - 1 > -1)
									LE += beta * (label_input_img.at<uchar>(x - 1, y) != cur_label);
								if(x + 1 < IMG_H)
									LE += beta * (label_input_img.at<uchar>(x + 1, y) != cur_label);
								if(y - 1 > -1)
									LE += beta * (label_input_img.at<uchar>(x, y - 1) != cur_label);
								if(y + 1 < IMG_W)
									LE += beta * (label_input_img.at<uchar>(x, y + 1) != cur_label);
	                        
	                        	/* Assign probability to each label this is used as weight */
								p_weights.push_back(expf(-(1 / T) * LE));
							}

	                    	/* Sample one label based on the weight->Proabaility of label */
							discrete_distribution<> weight(p_weights.begin(), p_weights.end());
							int rand_index = weight(gen);
							label_input_img.at<uchar>(x, y) = labels[rand_index];
						}
					}
				}
			}
		}
		Mat output_img = greyToRGB(label_input_img);
		tend = omp_get_wtime();
		cout << "Time taken for computation: " << tend - tstart << endl;
		cout << "Writing output file..." << endl;

		/* Write the pixel values to a csv file */
		writeToCSV(output_file_name, output_img);

		imwrite("resources/sample.jpg", output_img);
		cout << "Done" << endl;

	}

	return 1;
}
