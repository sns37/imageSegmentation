#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <math.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <cstdlib>
#include <ctime>

using namespace std;
using namespace cv;

// variables used in the disparity eqn
const double coef = 20;
unsigned char labels[5];
unsigned int scan_length;
unsigned char init_vect;
unsigned int iter_time;
unsigned int IMG_H, IMG_W;
double tstart, tend;

// initial image function
Mat initial_vector(Mat img_disp)
{
	for(int x=0; x<IMG_H; x++) {
		for(int y=0; y<IMG_W; y++) {
			img_disp.at<uchar>(y, x) = init_vect;
		}
	}
	return img_disp;
}

// write disparity value to a CSV file
void write_CSV(char* path, Mat label)
{
	FILE* fp = fopen(path, "w+");
	for (int x=0; x<IMG_H; x++) {
		for (int y=0; y<IMG_W; y++) {
			fprintf(fp, "%d,", label.at<uchar>(x, y));
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

// write output image file
Mat layering(Mat input)
{
	Mat output(IMG_H, IMG_W, CV_8UC3);
	for(int x=0; x<IMG_H; x++) {
		for(int y=0; y<IMG_W; y++) {
			if(input.at<uchar>(x, y) == labels[0]) {
				output.at<Vec3b>(x, y)[0] = 5;
				output.at<Vec3b>(x, y)[1] = 5;
				output.at<Vec3b>(x, y)[2] = 5;
			}
			else if(input.at<uchar>(x, y) == labels[1]) {
				output.at<Vec3b>(x, y)[0] = 67;
				output.at<Vec3b>(x, y)[1] = 67;
				output.at<Vec3b>(x, y)[2] = 67;
			}
			else if(input.at<uchar>(x, y) == labels[2]) {
				output.at<Vec3b>(x, y)[0] = 128;
				output.at<Vec3b>(x, y)[1] = 128;
				output.at<Vec3b>(x, y)[2] = 128;
			}
			else if(input.at<uchar>(x, y) == labels[3]) {
				output.at<Vec3b>(x, y)[0] = 189;
				output.at<Vec3b>(x, y)[1] = 189;
				output.at<Vec3b>(x, y)[2] = 189;
			}
			else if(input.at<uchar>(x, y) == labels[4]) {
				output.at<Vec3b>(x, y)[0] = 250;
				output.at<Vec3b>(x, y)[1] = 250;
				output.at<Vec3b>(x, y)[2] = 250;
			}
			else {
				output.at<Vec3b>(x, y)[0] = 255;
				output.at<Vec3b>(x, y)[1] = 0;
				output.at<Vec3b>(x, y)[2] = 0;
			}
		}
	}
	return output;
}

int main(int argc, char** argv)
{
	if(argc != 6){
		printf("Usage:./stereo_serial input_img_path_1 input_img_path_2 ");
		printf("output_image_path scan_length iteration_times\n");
		return 0;
	}

	string img_1_path = argv[1];
	string img_2_path = argv[2];
	string img_out_path = argv[3];
	scan_length = atoi(argv[4]);
	iter_time = atoi(argv[5]);

	// See random number generator using random device
	random_device rd;
	mt19937 gen(rd());

	//Check if the img is loaded properly
	if(img_1_path.empty() || img_2_path.empty()) {
		printf("\nIllegal input image path, END\n");
		return 0;
	}

	//Load Image from cmd arg
	Mat img_1 = imread(img_1_path, CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_2 = imread(img_2_path, CV_LOAD_IMAGE_GRAYSCALE);

	//Print image dimensions
	printf("Processing Stereo Vision\n");
	printf("Image 1 dimension: Height: %d, Width %d.\n", img_1.rows, img_1.cols);
	printf("Image 2 dimension: Height: %d, Width %d.\n", img_2.rows, img_2.cols);

	// check image dimension
	if(img_1.rows!=img_2.rows || img_1.cols!=img_2.cols) {
		printf("Image dimensions NOT match\n");
		return 0;
	}
	else {
		IMG_H = img_1.rows;
		IMG_W = img_1.cols;
	}

	// set initial vector values
	Mat img_disp(IMG_H, IMG_W, DataType<uchar>::type);
	init_vect = scan_length/2;	
	img_disp = initial_vector(img_disp);

	// set label values
	unsigned char label = scan_length/10;
	for(int i=0; i<5; i++) labels[i] = label*(i+1);

	// measuring the comutation time
	tstart = omp_get_wtime();

	for(int iter=0; iter<iter_time; iter++) {
		// process the black or white boxes of stencil
		// every black box in a row
		// #pragma omp parallel for
		for(int x=1; x<IMG_H-1; x++) {
			unsigned char first_pixel = x%2;
			for(int y=first_pixel; y<IMG_W-1; y+=2) {
				vector<double> pro_weight;
				double disparity;
				unsigned char length_count = 0;

				for(int k=y; k<IMG_W-1; k+=(scan_length/5)) {
					length_count ++;
					disparity = 0;

					// doubleton
					disparity += abs(double(k-y-img_disp.at<uchar>(x-1, y)));
					disparity += abs(double(k-y-img_disp.at<uchar>(x+1, y)));
					disparity += abs(double(k-y-img_disp.at<uchar>(x, y-1)));
					disparity += abs(double(k-y-img_disp.at<uchar>(x, y+1)));
					// singleton
					disparity += coef*pow(double(img_1.at<uchar>(x, y)-img_2.at<uchar>(x, k)), 2);
					// store each exponential disparity value
					pro_weight.push_back(expf(-(0.001)*disparity));
					if(length_count>=5) break;
				}
				discrete_distribution<> weight(pro_weight.begin(), pro_weight.end());
				img_disp.at<uchar>(x, y) = labels[weight(gen)];
			}
		}

		// every white box in a row
		// #pragma omp parallel for
		for(int x=1; x<IMG_H-1; x++) {
			unsigned char first_pixel = x%2;
			for(int y=first_pixel+1; y<IMG_W-1; y+=2) {
				vector<double> pro_weight;
				double disparity;
				unsigned char length_count = 0;

				for(int k=y; k<IMG_W-1; k+=(scan_length/5)) {
					length_count ++;
					disparity = 0;

					// doubleton
					disparity += abs(double(k-y-img_disp.at<uchar>(x-1, y)));
					disparity += abs(double(k-y-img_disp.at<uchar>(x+1, y)));
					disparity += abs(double(k-y-img_disp.at<uchar>(x, y-1)));
					disparity += abs(double(k-y-img_disp.at<uchar>(x, y+1)));
					// singleton
					disparity += coef*pow(double(img_1.at<uchar>(x, y)-img_2.at<uchar>(x, k)), 2);
					// store each exponential disparity value
					pro_weight.push_back(expf(-(0.001)*disparity));
					if(length_count>=5) break;
				}
				discrete_distribution<> weight(pro_weight.begin(), pro_weight.end());
				img_disp.at<uchar>(x, y) = labels[weight(gen)];
			}
		}
	}

	tend = omp_get_wtime();
	printf("Computation time is %f seconds\n", tend-tstart);

	// write output files and then done
	printf("Writing output file\n");
	write_CSV("files/result_values.csv", img_disp);
	img_disp = layering(img_disp);
	imwrite(img_out_path, img_disp);
	printf("Done\n");
	return 1;
}