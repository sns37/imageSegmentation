#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <math.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <cstdlib>
#include <ctime>

using namespace std;
using namespace cv;

// variables used in the disparity eqn
const double coef = 0.05;
const unsigned char scan_length = 20;
const unsigned char init_vect = scan_length/2;	// assuming pixels only have right movement
// const unsigned char label_1 = scan_length/5;
// const unsigned char label_2 = label_1*2;
// const unsigned char label_3 = label_1*3;
// const unsigned char label_4 = label_1*4;
const unsigned char label_1 = 1;
const unsigned char label_2 = 2;
const unsigned char label_3 = 4;
const unsigned char label_4 = 8;
unsigned int IMG_H, IMG_W;
double tstart, tend;

// initial image function
Mat initial_vector(Mat img_disp)
{
	for(int i=0; i<IMG_H; i++) {
		for(int j=0; j<IMG_W; j++) {
			img_disp.at<uchar>(i, j) = init_vect;
		}
	}
	return img_disp;
}

// write disparity value to a CSV file
void write_CSV(char* path, Mat label)
{
	FILE* fp = fopen(path, "w+");
	for (int i=0; i<IMG_H; i++) {
		for (int j=0; j<IMG_W; j++) {
			fprintf(fp, "%d,", label.at<uchar>(i, j));
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

// write output image file
Mat layering(Mat input)
{
	Mat output(IMG_W, IMG_H, CV_8UC3);
	for(int i=0; i<IMG_H; i++) {
		for(int j=0; j<IMG_W; j++) {
			if(input.at<uchar>(i, j) < scan_length) {
				output.at<Vec3b>(i, j)[0] = 42;
				output.at<Vec3b>(i, j)[1] = 42;
				output.at<Vec3b>(i, j)[2] = 42;
			}
			if(input.at<uchar>(i, j) < label_4) {
				output.at<Vec3b>(i, j)[0] = 85;
				output.at<Vec3b>(i, j)[1] = 85;
				output.at<Vec3b>(i, j)[2] = 85;
			}
			if(input.at<uchar>(i, j) < label_3) {
				output.at<Vec3b>(i, j)[0] = 128;
				output.at<Vec3b>(i, j)[1] = 128;
				output.at<Vec3b>(i, j)[2] = 128;
			}
			if(input.at<uchar>(i, j) < label_2) {
				output.at<Vec3b>(i, j)[0] = 171;
				output.at<Vec3b>(i, j)[1] = 171;
				output.at<Vec3b>(i, j)[2] = 171;
			}
			if(input.at<uchar>(i, j) < label_1) {
				output.at<Vec3b>(i, j)[0] = 213;
				output.at<Vec3b>(i, j)[1] = 213;
				output.at<Vec3b>(i, j)[2] = 213;
			}
		}
	}
	return output;
}

int main(int argc, char** argv)
{
	if(argc != 5){
		printf("Usage:./stereo_serial input_img_path_1 input_img_path_2 ");
		printf("output_image_path iteration_times\n");
		return 0;
	}

	string img_1_path = argv[1];
	string img_2_path = argv[2];
	string img_out_path = argv[3];
	unsigned int iter_time = atoi(argv[4]);

	// See random number generator using random device
	random_device rd;
	mt19937 gen(rd());

	//Check if the img is loaded properly
	if(img_1_path.empty() && img_2_path.empty()) {
		printf("\nIllegal input image path, END\n");
		return 0;
	}

	//Load Image from cmd arg
	Mat img_1 = imread(img_1_path, CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_2 = imread(img_2_path, CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_disp = imread(img_2_path, CV_LOAD_IMAGE_GRAYSCALE);

	//Print image dimensions
	printf("Processing Stereo Vision\n");
	printf("Image 1 dimension: Height: %d, Width %d.\n", img_1.rows, img_1.cols);
	printf("Image 2 dimension: Height: %d, Width %d.\n", img_2.rows, img_2.cols);

	// check image dimension
	if(img_1.rows!=img_2.rows || img_1.cols!=img_2.cols) {
		printf("Image dimensions NOT match\n");
		return 0;
	}
	else {
		IMG_H = img_1.rows;
		IMG_W = img_2.cols;
	}
		
	// write to CSV file to test
	img_disp = initial_vector(img_disp);

	// measuring the comutation time
	tstart = omp_get_wtime();

	for(int iter=0; iter<iter_time; iter++) {
		// process the black or white boxes of stencil
		// every rows in a col
		for(int i=0; i<IMG_H; i++) {

			// every black box in a row
			#pragma omp parallel for
			for(int j=0; j<IMG_W; j+=2) {

				// every scanning pixel for singleton
				// if the pixel is close to the right edge

				double disparity;
				unsigned char counter;
				double sum = 0;

				for(int k=i; k<IMG_W && k-i<scan_length; k++) {
					disparity = 0;
					counter = 0;

					// doubleton
					if(i>0) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i-1, j)));
						counter ++;
					}
					if(i<IMG_W-1) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i+1, j)));
						counter ++;
					}
					if(j>0) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i, j-1)));
						counter ++;
					}
					if(j<IMG_H-1) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i, j+1)));
						counter ++;
					}
					disparity /= 3*double(counter);

					// singleton
					disparity += coef*pow(double(img_1.at<uchar>(i, j)-img_2.at<uchar>(k, j)), 2);
					
					sum += disparity/100;
				}
				img_disp.at<uchar>(i, j) = sum;
			}

			// every white box in a row
			#pragma omp parallel for
			for(int j=1; j<IMG_W; j+=2) {

				// every scanning pixel for singleton
				// if the pixel is close to the right edge

				double disparity;
				unsigned char counter;
				double sum = 0;

				for(int k=i; k<IMG_W && k-i<scan_length; k++) {
					disparity = 0;
					counter = 0;
					
					// doubleton
					if(i>0) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i-1, j)));
						counter ++;
					}
					if(i<IMG_W-1) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i+1, j)));
						counter ++;
					}
					if(j>0) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i, j-1)));
						counter ++;
					}
					if(j<IMG_H-1) {
						disparity += abs(double(k-i-img_disp.at<uchar>(i, j+1)));
						counter ++;
					}
					disparity /= 3*double(counter);

					// singleton
					disparity += coef*pow(double(img_1.at<uchar>(i, j)-img_2.at<uchar>(k, j)), 2);
					
					sum += disparity/100;
				}
				img_disp.at<uchar>(i, j) = sum;
			}
		}
	}

	tend = omp_get_wtime();
	printf("Computation time is %f seconds\n", tend-tstart);

	// write output files and then done
	printf("Writing output file\n");
	write_CSV("files/output_values.csv", img_disp);
	img_disp = layering(img_disp);
	imwrite(img_out_path, img_disp);
	printf("Done\n");
	return 1;
}