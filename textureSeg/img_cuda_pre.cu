#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <random>
#include <math.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <cstdlib>
#include <ctime>
#include <omp.h>
#include <curand.h>
#include <curand_kernel.h>

using namespace std;
using namespace cv;

/* Constants used in the energy eqn*/
const double beta = 0.5*2;
const double var = 0.5;
__device__ __constant__ double wt_clq_n = 0.05, wt_clq_e = 0.05, wt_clq_ne = 0.05, wt_clq_se = 0.05, wt_clq_top = 0.05, wt_clq_right = 0.05;
__device__ __constant__ unsigned char labels[5] = {0, 63, 127, 191, 255};
const int max_iter = 5000;
const int noOfSamples = 1;
#define BLACK 1
#define WHITE 2

/* GPU parameters 
 * Blocks of 1024 threads 
 * Assuming biggest image is of 2048 * 2048, we have 4096 total blocks
 * Assuming 2D blocks and 2D grids, we have 64 blocks per grid and 32 threads per block
 * pixel i = (blockIdx.x * blockDim.x) + threadIdx.x;
 * pixel j = (blockIdx.y * blockDim.y) + threadIdx.y;
 */
#define THREADS_PER_BLOCK 256
#define BLOCKS_PER_GRID 2048
 __device__  int *singleton_E;
 __device__ int D_IMG_W, D_IMG_H;

/* Annealing Parameters */
const double T = 0.1;

/* Image Data */
//int IMG_H, IMG_W;
#define IMG_W 256
#define IMG_H 256
char* output_file_name = "./files/texture_final.csv";
int curSample = 0;
Mat samples[noOfSamples];

/* Time values */
double tstart, tend;

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void initLabelImg(unsigned char* input_img, unsigned char* label_input_img)
{
    // uint j = (blockIdx.y * blockDim.y) + threadIdx.y;
 //    uint i = (blockIdx.x * blockDim.x) + threadIdx.x;
    int threadId = blockIdx.x *blockDim.x + threadIdx.x;
    int i = threadId/IMG_H, j = threadId % IMG_W;
    
    if(i >= 0 && j >= 0 && i < IMG_H && j < IMG_W){
            if(input_img[i * IMG_W + j] < 223)
                label_input_img[i * IMG_W + j] = 191;

            if(input_img[i * IMG_W + j] < 159)
                label_input_img[i * IMG_W + j] = 127;

            if(input_img[i * IMG_W + j] < 95)
                label_input_img[i * IMG_W + j] = 63;

            if(input_img[i * IMG_W + j] < 31)
                label_input_img[i * IMG_W + j] = 0;
    }
}



__global__ void preCalSingletonEnergy(unsigned char* input_img){

    int threadId = blockIdx.x *blockDim.x + threadIdx.x;
    int i = threadId/IMG_H, j = threadId % IMG_W;

    if(i >= 0 && j >= 0 && i < IMG_H && j < IMG_W){
        for(int iter_label = 0; iter_label < 5; iter_label++){
            unsigned char cur_label = labels[iter_label];

            singleton_E[i * IMG_W + j * IMG_H + iter_label] = 0;

            if(j - 2 > -1)
                singleton_E[i * IMG_W + j * IMG_H + iter_label] += wt_clq_top * abs(cur_label - input_img[i * IMG_W + (j - 2)]);
            if(j - 1 > -1)
                singleton_E[i * IMG_W + j * IMG_H + iter_label] += wt_clq_n * abs(cur_label - input_img[i * IMG_W + (j - 1)]);
            if(i + 1 < IMG_W && j - 1 > -1)
                singleton_E[i * IMG_W + j * IMG_H + iter_label] += wt_clq_ne * abs(cur_label - input_img[(i + 1) * IMG_W + (j - 1)]);
            if(i + 1 < IMG_W)
                singleton_E[i * IMG_W + j * IMG_H + iter_label] += wt_clq_e * abs(cur_label - input_img[(i + 1) * IMG_W + j]);
            if(i + 2 < IMG_W)
                singleton_E[i * IMG_W + j * IMG_H + iter_label] += wt_clq_right * abs(cur_label - input_img[(i + 2) * IMG_W + j]);
            if(i + 1 < IMG_W)
                singleton_E[i * IMG_W + j * IMG_H + iter_label] += wt_clq_se * abs(cur_label - input_img[(i + 1) * IMG_W + j]);

            singleton_E[i * IMG_W + j * IMG_H + iter_label] /= (2 * var);
        }
    }
}

__global__ void computeBlackLabels(unsigned char* input_img, unsigned char* label_input_img, float* devRanNos){

  /*  uint j = (blockIdx.y * blockDim.y) + threadIdx.y;
    uint i = (blockIdx.x * blockDim.x) + threadIdx.x;*/
   int threadId = blockIdx.x *blockDim.x + threadIdx.x;
   int i = threadId/IMG_H, j = threadId % IMG_W;

    if(i >= 0 && j >= 0 && i < IMG_H && j < IMG_W){

        int x = i, y = (2 * j) + (i % 2);

        if(x >= 0 && y >= 0 && x < IMG_H && y < IMG_W){
            /* Calculate the Local Energy(LE) for 5 labels given energy of neighbours */
            /* Use Gibbs distribution to obtain probability of taking a given label */
            double label_prob[5];
            double LE= 0;
            // printf("%d %d\n", x, y);

            for(int iter_label = 0; iter_label < 5; iter_label++){
                unsigned char cur_label = labels[iter_label];

                /* Calculate energy of singleton */
                double LE = singleton_E[x * IMG_W + IMG_H * y + iter_label];
                
                /* Caluculate energy of doubleton = diff(Pixel & Neighbours) */
                if(x - 1 > -1)
                    LE += beta * (label_input_img[(x - 1) * IMG_W + y] != cur_label);
                if(x + 1 < IMG_H)
                    LE += beta * (label_input_img[(x + 1) * IMG_W + y] != cur_label);
                if(y - 1 > -1)
                    LE += beta * (label_input_img[x * IMG_W + (y - 1)] != cur_label);
                if(y + 1 < IMG_W)
                    LE += beta * (label_input_img[x * IMG_W + (y - 1)] != cur_label);
                
                /* Assign probability to each label this is used as weight */
                label_prob[iter_label] = expf(-(1 / T) * LE);
            }
            
            for (int k = 1; k < 5; k++) {
                label_prob[k] = label_prob[k - 1] + label_prob[k];
            }

            float rand_num = label_prob[4] * devRanNos[x * IMG_W + y];
            // printf("%f\t", devRanNos[x * IMG_W + y]);

            int label_index = 0;
            for (int k = 0; k < 4; k++) {
                if ((rand_num > label_prob[k]) && (rand_num < label_prob[k + 1])) {
                    label_index = k + 1;
                    break;
                }
            }
            
            label_input_img[x * IMG_W + y] = labels[label_index];
        }
    }
}

__global__ void computeWhiteLabels(unsigned char* input_img, unsigned char* label_input_img, float* devRanNos){

    // uint i = (blockIdx.x * blockDim.x) + threadIdx.x;
    // uint j = (blockIdx.y * blockDim.y) + threadIdx.y;
    int threadId = blockIdx.x *blockDim.x + threadIdx.x;
    int i = threadId/IMG_H, j = threadId % IMG_W;
    
    if(i >= 0 && j >= 0 && i < IMG_H && j < IMG_W){

        int x = i, y = (2 * j) + (i + 1) % 2;

        if(x >= 0 && y >= 0 && x < IMG_H && y < IMG_W){
            /* Calculate the Local Energy(LE) for 5 labels given energy of neighbours */
            /* Use Gibbs distribution to obtain probability of taking a given label */
            double label_prob[5];
            double LE = 0;
            // printf("%d %d\n", x, y);

            for(int iter_label = 0; iter_label < 5; iter_label++){
                unsigned char cur_label = labels[iter_label];

                /* Calculate energy of singleton */
                double LE = singleton_E[x * IMG_W + IMG_H * y + iter_label];
                
                /* Caluculate energy of doubleton = diff(Pixel & Neighbours) */
                if(x - 1 > -1)
                    LE += beta * (label_input_img[(x - 1) * IMG_W + y] != cur_label);
                if(x + 1 < IMG_H)
                    LE += beta * (label_input_img[(x + 1) * IMG_W + y] != cur_label);
                if(y - 1 > -1)
                    LE += beta * (label_input_img[x * IMG_W + (y - 1)] != cur_label);
                if(y + 1 < IMG_W)
                    LE += beta * (label_input_img[x * IMG_W + (y - 1)] != cur_label);
                
                /* Assign probability to each label this is used as weight */
                label_prob[iter_label] = exp(-(1 / T) * LE);
                //if(x == 1 && y == 1 || x == 1 && y == 2  || x == 1 && y == 3)   printf(" --iter=%d LE=%f   prob=%f -- ", iter_label, LE, label_prob[iter_label]);
            }
            // if(x == 1 && y == 1 || x == 1 && y == 2  || x == 1 && y == 3)
            //     for (int k = 1; k < 5; k++) {
            //        printf("prob = %d", label_prob[k-1]);
            //     }                

            for (int k = 1; k < 5; k++) {
                label_prob[k] = label_prob[k - 1] + label_prob[k];
            }
            //printf("%f \t", label_prob[4]);

            float rand_num = label_prob[4] * devRanNos[x * IMG_W + y];

            int label_index = 0;
            for (int k = 0; k < 4; k++) {
                if ((rand_num > label_prob[k]) && (rand_num < label_prob[k + 1])) {
                    label_index = k + 1;
                    break;
                }
            }
            // printf("%d\t", label_index);
            label_input_img[x * IMG_W + y] = labels[label_index];
            //if(x == 1 && y == 1 || x == 1 && y == 2  || x == 1 && y == 3)
            //printf("\n-----value=%d index =%d rand=%f sum=%f no=%d\n", label_input_img[x * IMG_W + y], label_index, rand_num, label_prob[4], devRanNos[x * IMG_W + y]);    
            //for(int k = 0 ; k < 100; k++) printf("  %f  ", devRanNos[x * IMG_W + IMG_H]);
        }
    }
}

void writeToCSV(char* path, Mat label)
{
        /* Write the pixel values to a csv file */
        FILE* fp = fopen(path, "w+");
        for (int i = 0; i < IMG_H; i++) {
            for (int j = 0; j < IMG_W; j++) {
                fprintf(fp, "%d,", label.at<uchar>(i, j));
            }
            fprintf(fp, "\n");
        }
        fclose(fp);
}

Mat greyToRGB(Mat input)
{
    Mat output(IMG_W, IMG_H, CV_8UC3); 
    unsigned char label = 0;

    #pragma omp parallel for collapse(2)
    for(int x = 0; x < IMG_H; x++){
        for(int y = 0; y < IMG_W; y++){
            label = input.at<uchar>(x, y);

            switch(label){
                /* RED Colored Pixel */
                case 0:
                    output.at<Vec3b>(x, y)[0] = 255;
                    output.at<Vec3b>(x, y)[1] = 0;
                    output.at<Vec3b>(x, y)[2] = 0;
                break;

                /* GREEN Colored Pixel */
                case 63:
                    output.at<Vec3b>(x, y)[0] = 124;
                    output.at<Vec3b>(x, y)[1] = 252;
                    output.at<Vec3b>(x, y)[2] = 0;
                break;

                /* BLUE Coloured Pixel */
                case 127:
                output.at<Vec3b>(x, y)[0] = 65;
                    output.at<Vec3b>(x, y)[1] = 105;
                    output.at<Vec3b>(x, y)[2] = 255;
                break;

                /* YELLOW Coloured Pixel */
                case 191:
                    output.at<Vec3b>(x, y)[0] = 255;
                    output.at<Vec3b>(x, y)[1] = 215;
                    output.at<Vec3b>(x, y)[2] = 0;
                break;

                /* MILD BROWN Coloured Pixel */
                case 255:
                    output.at<Vec3b>(x, y)[0] = 0;
                    output.at<Vec3b>(x, y)[1] = 0;
                    output.at<Vec3b>(x, y)[2] = 0;
                break;
            }
        }
    }

    return output;
}

//////////////// EXTRA FUNCTIONS /////////////////////
void storeSample(Mat sample, int no, int curIter){
    if (curIter % (max_iter/noOfSamples) == 0) {
        printf("%d r=%d, c=%d, nr=%d, nc=%d\n", curIter, sample.rows, sample.cols, samples[no].rows, samples[no].cols);
        samples[no] = sample.clone();
        curSample++;
    } 
}

Mat meanOfSamples(Mat input_img, int type){

    if(type == BLACK){
        
        #pragma omp parallel for collapse(2)
        for(int black_i = 0; black_i < IMG_H; black_i++){
            for(int black_j = 0; black_j < (IMG_W/2); black_j++){
                int x = black_i, y = (2 * black_j) + (black_i % 2);

                if(x > -1 && y > -1 && x < (input_img.rows) && y < (input_img.cols)){

                    int sum = 0;
                    for(int count = 0 ; count < noOfSamples ; count++)
                        sum += samples[count].at<uchar>(x, y);

                    input_img.at<uchar>(x, y) = sum/noOfSamples;
                }
            }
        }
    }
    else if(type == WHITE){

        #pragma omp parallel for collapse(2)
        for(int white_i = 0; white_i < IMG_H; white_i++){
            for(int white_j = 0; white_j < (IMG_W/2); white_j++){
                int x = white_i, y = (2 * white_j) + ((white_i + 1) % 2);

                if(x > -1 && y > -1 && x < input_img.rows && y < input_img.cols){

                    int sum = 0;
                    for(int count = 0 ; count < noOfSamples ; count++)
                        sum += samples[count].at<uchar>(x, y);

                    input_img.at<uchar>(x, y) = sum/noOfSamples;
                }
            }
        }
    }

    return input_img;
}
/////////////////////////////////////////////////////


int main(int argc, char* argv[])
{
    if(argc != 2){
        printf("\nPlease input image as an argument\n");
        return 0;
    }

    string img_path1 = argv[1];
    
    /* See random number generator using random device */
    float *devRanNos;
    curandGenerator_t gen;
    cudaMalloc((void **)&devRanNos, IMG_W * IMG_H * sizeof(float));
    curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT); 
    curandSetPseudoRandomGeneratorSeed(gen, omp_get_wtime());
    curandGenerateUniform(gen, devRanNos, IMG_W * IMG_H);
    
    // curandState *d_state; 
    // cudaMalloc(&d_state, sizeof(curandState));
    // unsigned *d_result;
    // unsigned *d_max_rand_int, *d_min_rand_int;
    // cudaMalloc(&d_result, (MAX-MIN+1) * sizeof(unsigned));
    // cudaMalloc(&d_max_rand_int, sizeof(unsigned));
    // cudaMalloc(&d_min_rand_int, sizeof(unsigned));
    // cudaMemset(d_result, 0, (MAX-MIN+1)*sizeof(unsigned));
    // setup_kernel<<<1,1>>>(d_state);
    // generate_kernel<<<1,1>>>(d_state, ITER, d_max_rand_int, d_min_rand_int, d_result);
      
    if(!img_path1.empty()) {
        //Load Image from cmd arg
        Mat input_img = imread(img_path1, CV_LOAD_IMAGE_GRAYSCALE);
        
        //Check if the img is loaded properly
        if(input_img.empty()) {
            CV_Error(CV_StsBadArg, "Sample image is empty. Please adjust your path, so it points to a valid input image!");
        }
        
        //Print image dimensions
        printf("Starting.... \nImage Loaded: Height: %d, Width %d.\n", input_img.rows, input_img.cols);
        //IMG_H = input_img.rows, IMG_W = input_img.cols;
        
        //Create a new 2D Mat for grey scale and set its pixels to 255
        Mat label_input_img(IMG_W, IMG_H, CV_8UC1);
        label_input_img.setTo(Scalar(255));

        /*Allocate space on GPU */
        unsigned char *data_label_input_img, *data_input_img;

        gpuErrchk(cudaMalloc(&data_input_img, sizeof(unsigned char) * IMG_H * IMG_W));
        gpuErrchk(cudaMalloc(&data_label_input_img, sizeof(unsigned char) * IMG_H * IMG_W));
        gpuErrchk(cudaMemcpy(data_input_img, (unsigned char *) input_img.data, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyHostToDevice));
        gpuErrchk(cudaMemcpy(data_label_input_img, (unsigned char *) label_input_img.data, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyHostToDevice));
        gpuErrchk(cudaMalloc(&singleton_E, sizeof(unsigned char) * IMG_H * IMG_W * 5));
        gpuErrchk(cudaMemset(singleton_E, 0, IMG_W * IMG_W * 5));

        /* start measuring the comutation time */
        tstart = omp_get_wtime();
        
        /* Make 2D grid of 2D blocks*/
        dim3 threadsPerBlock(THREADS_PER_BLOCK, THREADS_PER_BLOCK);
        dim3 numBlocks(BLOCKS_PER_GRID, BLOCKS_PER_GRID); 

        initLabelImg<<<BLOCKS_PER_GRID, THREADS_PER_BLOCK>>>(data_input_img, data_label_input_img);
        preCalSingletonEnergy<<<BLOCKS_PER_GRID, THREADS_PER_BLOCK>>>(data_label_input_img);
        gpuErrchk(cudaPeekAtLastError());
        cudaDeviceSynchronize();

        #pragma omp unroll
        for(int cur_iter = 0; cur_iter < max_iter; cur_iter++){
            //curandSetPseudoRandomGeneratorSeed(gen, omp_get_wtime());
            //curandGenerateUniform(gen, devRanNos, 1);
            computeBlackLabels<<<BLOCKS_PER_GRID, THREADS_PER_BLOCK>>>(data_input_img, data_label_input_img, devRanNos);
            // printf("\n\n\n\n\n\n");
            cudaThreadSynchronize();
            computeWhiteLabels<<<BLOCKS_PER_GRID, THREADS_PER_BLOCK>>>(data_input_img, data_label_input_img, devRanNos);
            //computeBlackLabels<<<1, 1>>>(data_input_img, data_label_input_img, devRanNos);
            //computeWhiteLabels<<<1, 1>>>(data_input_img, data_label_input_img, devRanNos);
            //gpuErrchk(cudaPeekAtLastError());
        }
        cudaDeviceSynchronize();
        tend = omp_get_wtime();
        gpuErrchk(cudaMemcpy(label_input_img.data, data_label_input_img, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyDeviceToHost));       

        int total = tend - tstart;
        tstart = omp_get_wtime();
        Mat output_img = greyToRGB(label_input_img);
        tend = omp_get_wtime();

        
        cout << "Time taken for computation: " << total + tend - tstart << endl;
        cout << "Writing output image of size  " << output_img.rows << " * " <<  output_img.cols << endl;

        /* Write the pixel values to a csv file */
        writeToCSV(output_file_name, output_img);

        imwrite("files/output_texture.jpg", output_img);
        cout << "Done" << endl;


        curandDestroyGenerator(gen);
        gpuErrchk(cudaFree(data_label_input_img));
        gpuErrchk(cudaFree(singleton_E));
        gpuErrchk(cudaFree(devRanNos));
    }

    return 0;
}