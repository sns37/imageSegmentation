#ifndef MACRO_DEFINE_H_
#define MACRO_DEFINE_H_

#define MAX_MEM_FOR_RAND 4*1024*1024*1024
#define MAX_BLOCK_NUM 8*14

//#define HEIGHT 1080
//#define WIDTH 1920
//#define PIXEL_PER_THREAD_2 6
//#define THREAD_PER_BLOCK 576

 #define HEIGHT 320
 #define WIDTH 320
 #define PIXEL_PER_THREAD_2 5
 #define THREAD_PER_BLOCK 512

#define PIXEL_PER_THREAD_1 4
#define GPU_SPEC false
#define RAW_IMAGE_SHOW false
#define PROFILING false

#endif
