/*
 * rand_gen.cuh
 *
 *  Created on: Mar 13, 2014
 *      Author: yuxuan
 */

#ifndef RAND_GEN_CUH_
#define RAND_GEN_CUH_

void rand_gen_batch(float * random_numbers, int height, int width, int multiplier, int i);

#endif /* RAND_GEN_CUH_ */
