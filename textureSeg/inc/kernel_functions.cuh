#ifndef RAND_GEN_CUH_
#define RAND_GEN_CUH_

void set_const(unsigned char* labels_h, float beta_h, float var_h, float c_h);
void rand_gen_batch(float * random_numbers, int multiplier, int i);

__device__ void distance_cal1(unsigned char old_intensity, unsigned char *img2, int *new_locs, float *LE);

__device__ void distance_cal2(int label_index, int *distance);

__global__ void initial_labeling(unsigned char *raw_data_device, unsigned char *data, unsigned char *raw_label_device);

__global__ void initial_reformat(unsigned char *img1_input, unsigned char *img1_output);

__global__ void initial_labeling(unsigned char *raw_data_device, unsigned char *raw_label_device);

__global__ void singleton_calculation1(unsigned char *img1, unsigned char *img2, float *singleton);

__global__ void singleton_calculation1(unsigned char *img, float *singleton);

__global__ void singleton_calculation2(unsigned char *img1, unsigned char *img2, int * singleton_storet);

__global__ void iterative_labeling_mode1_black(float * singleton, unsigned char *old_label_device, float T, float *random_numbers);

__global__ void iterative_labeling_mode1_black_storet(unsigned char * img1, unsigned char * img2, int *label);

__global__ void iterative_labeling_mode1_white(float * singleton, unsigned char *old_label_device, float T, float *random_numbers);

__global__ void iterative_labeling_mode1_black_storet(unsigned char * data, unsigned char *old_label_device);

__global__ void iterative_labeling_mode1_white_storet(unsigned char * data, unsigned char *old_label_device);

__global__ void final_reformat(int *label, int *final_label);

__global__ void iterative_labeling_mode1_black(unsigned char *data, unsigned char *old_label_device, float T, float *random_numbers);

__global__ void iterative_labeling_mode1_white(unsigned char *data, unsigned char *old_label_device, float T, float *random_numbers);

__global__ void iterative_labeling_mode1_white(unsigned char * img1, unsigned char * img2, int *label, float T, float *random_numbers);

__global__ void final_labeling(unsigned char * raw_label_device, unsigned char * final_label_device);
#endif /* RAND_GEN_CUH_ */
