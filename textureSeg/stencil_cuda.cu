#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <curand.h>
#include <math.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <omp.h>
#include <cstdlib>
#include <ctime>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include "omp.h"

#include "./inc/helper_cuda.h"  //cuda error checking
#include "./inc/kernel_functions.cuh"

using namespace std;
using namespace cv;

/* Constants used in the energy eqn*/
const double beta = 70;
const double var = 1;
const double wt_clq =1;
__constant__ unsigned char labels[5]= {90, 115, 140, 165, 190};
const int max_iter = 5000;

/* Annealing Parameters */
const double T = 5000000;

/* Image Data */
int IMG_H, IMG_W;

//unsigned char* output_file_name = "./files/texture_final.csv";

/* Time values */
double tstart, tend;

#define PIXEL_PER_THREAD_1 4
#define PIXEL_PER_THREAD_2 5
#define THREAD_PER_BLOCK 512
#define MAX_MEM_FOR_RAND 4*1024*1024*1024

__global__ void initLabelImg(int IMG_W, int IMG_H, unsigned char *label_input_img)
{
	int pixel_index = threadIdx.x + blockDim.x * blockIdx.x;
		if(pixel_index < IMG_W*IMG_H){

			label_input_img[pixel_index] = 150;
		}

}

void rand_gen_batch(float * random_numbers, int multiplier) {
	curandGenerator_t gen;
	checkCudaErrors(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
	checkCudaErrors(curandSetPseudoRandomGeneratorSeed(gen, 1));

	// curandStatus_t curandStatus;
	// curandStatus = curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT);
	// curandStatus = curandSetPseudoRandomGeneratorSeed(gen, 1);
	// checkCudaErrors(
	// 		curandSetPseudoRandomGeneratorSeed(gen, (unsigned long long)(time(NULL)+i)));
	checkCudaErrors(curandGenerateUniform(gen, random_numbers, IMG_W * IMG_H * multiplier));
	return;
}

void writeToCSV(unsigned char *label_input_img)
{
		/* Write the pixel values to a csv file */
		FILE* fp = fopen("./inc/final.csv", "w+");
		for (int i = 0; i < IMG_H; i++) {
			for (int j = 0; j < IMG_W; j++) {
				fprintf(fp, "%d,", label_input_img[i*IMG_W+j]);
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
}

Mat greyToRGB(unsigned char *label_input_img)
{
	Mat output(IMG_W, IMG_H, CV_8UC3); 
	unsigned char label = 0;

	for(int x = 0; x < IMG_H; x++){
		for(int y = 0; y < IMG_W; y++){
			label = label_input_img[x * IMG_W + y];

            switch(label){
                    /* RED Colored Pixel */
                case 90:
                    output.at<Vec3b>(x, y)[0] = 255;
                    output.at<Vec3b>(x, y)[1] = 0;
                    output.at<Vec3b>(x, y)[2] = 0;
                    break;
                    
                    /* GREEN Colored Pixel */
                case 115:
                    output.at<Vec3b>(x, y)[0] = 124;
                    output.at<Vec3b>(x, y)[1] = 252;
                    output.at<Vec3b>(x, y)[2] = 0;
                    break;
                    
                    /* BLUE Coloured Pixel */
                case 140:
                    output.at<Vec3b>(x, y)[0] = 65;
                    output.at<Vec3b>(x, y)[1] = 105;
                    output.at<Vec3b>(x, y)[2] = 255;
                    break;
                    
                    /* YELLOW Coloured Pixel */
                case 165:
                    output.at<Vec3b>(x, y)[0] = 255;
                    output.at<Vec3b>(x, y)[1] = 215;
                    output.at<Vec3b>(x, y)[2] = 0;
                    break;
                    
                    /* MILD BROWN Coloured Pixel */
                case 190:
                    output.at<Vec3b>(x, y)[0] = 0;
                    output.at<Vec3b>(x, y)[1] = 0;
                    output.at<Vec3b>(x, y)[2] = 0;
                    break;
            }
		}
	}

	return output;
}

__global__ void Black(int IMG_W, int IMG_H, float* random_numbers, unsigned char *input_img1, unsigned char *label_input_img){
	float label_prob[5];
	int black_i = blockIdx.y * blockDim.y + threadIdx.y;
	int black_j = blockIdx.x * blockDim.x + threadIdx.x;

//	        for(int black_i = 0; black_i < IMG_H/15; black_i++){
//	            for(int black_j = 0; black_j < IMG_W/30; black_j++){
	        if (black_i<IMG_H/15){
	        	if(black_j<IMG_W/30){
	                int x,y;
	                if(black_i%2==0){
	                    x = black_i*15+7;
	                    y = black_j*30+7;
	                    }
	                else{
	                    x = black_i*15+7;
	                    y = black_j*30+22;
	                    }
	            if(x>6&&x<IMG_H-7&&y>6&&y<IMG_W-7){
	            	for(int iter_label = 0; iter_label < 5; iter_label++){
	            		unsigned char cur_label = labels[iter_label];
	                    double LE=0;
						#pragma unroll
	                    for(int k=-7;k<8;k++){
							#pragma unroll
	                    	for(int h=-7;h<8;h++){
	                            LE+=input_img1[x*IMG_W+y];
	//                            LE+= wt_clq * (input_img1.at<uchar>(x, y)-cur_label)*(input_img1.at<uchar>(x, y)-cur_label)/(2 * var);
	                        }
	                    }
	//                    printf("Black_LE_int1=%f\n",LE);
	                    LE= wt_clq * (LE-cur_label*225)*(LE-cur_label*225)/(2 * var);

	                    if(x - 14 > - 1 )LE += beta * abs (label_input_img[(x - 14) * IMG_W + y] - cur_label*225);
	                    if(x + 14 < IMG_H )LE += beta * abs (label_input_img[(x + 14) * IMG_W + y] - cur_label*225);
	                    if(y - 14 > -1)LE += beta * abs (label_input_img[x * IMG_W + y - 14] - cur_label*225);
	                    if(y + 14 < IMG_W)LE += beta * abs (label_input_img[x * IMG_W + y + 14] - cur_label*225);
	//                    printf("Black_iter_label=%d,  LE=%f, expf(-(1 / T) * LE)=%f\n",iter_label, LE,expf(-(1 / T) * LE));
	                    label_prob[iter_label] = __expf(-(1 / T) * LE);
	                }
	                for (int k = 1; k < 5; k++) {
	                			label_prob[k] = label_prob[k - 1] + label_prob[k];
	                }

	                float rand_num = label_prob[4]*random_numbers[x*IMG_W+y];

	             	int label_index = 0;
	                	for (int w = 0; w < 4; w++) {
	                		if ((rand_num > label_prob[w]) && (rand_num < label_prob[w + 1])) {
	                				label_index = w + 1;
	                				break;
	                			}
	                	}
					#pragma unroll
	                for(int k=-7;k<8;k++){
						#pragma unroll
	                	for(int h=-7;h<8;h++){
	                        label_input_img[ (x+k) * IMG_W + y + h ] = labels[label_index];
	//                        printf("lala_black,label_input_img.at<uchar>(x, y)=%d\n",label_input_img.at<uchar>(x, y));
	                    }
	                }
	            }
	            }
	        }
}

__global__ void White(int IMG_W, int IMG_H, float* random_numbers, unsigned char *input_img1, unsigned char *label_input_img){
	float label_prob[5];
	int white_i = blockIdx.y * blockDim.y + threadIdx.y;
	int white_j = blockIdx.x * blockDim.x + threadIdx.x;

	    	if (white_i<IMG_H/15){
	    		if(white_j<IMG_W/30){
	            int x,y;
	            	if(white_i%2==0){
	                    x = white_i*15+7;
	                    y = white_j*30+22;
	                }
	                else{
	                    x = white_i*15+7;
	                    y = white_j*30+7;
	                }
	            if(x>6&&x<IMG_H-7&&y>6&&y<IMG_W-7){
	            	for(int iter_label = 0; iter_label < 5; iter_label++){
	            		unsigned char cur_label = labels[iter_label];
	                    double LE=0;
							#pragma unroll
	                    	for(int k=-7;k<8;k++){
								#pragma unroll
	                    		for(int h=-7;h<8;h++){
	                            	LE+=input_img1[x*IMG_H+y];
	                            }
	                        }
	//                        printf("White_LE_int2=%f\n",LE);
	                    LE= wt_clq * (LE-cur_label*225)*(LE-cur_label*225)/(2 * var);
	                    if(x - 14 > - 1 )LE += beta * abs (label_input_img[(x - 14) * IMG_W + y] - cur_label*225);
	                    if(x + 14 < IMG_H )LE += beta * abs (label_input_img[(x + 14) * IMG_W + y] - cur_label*225);
	                    if(y - 14 > -1)LE += beta * abs (label_input_img[x * IMG_W + y - 14] - cur_label*225);
	                    if(y + 14 < IMG_W)LE += beta * abs (label_input_img[x * IMG_W + y + 14] - cur_label*225);
	//                        printf("White_iter_label=%d,  LE=%f, expf(-(1 / T) * LE)=%f\n",iter_label, LE,expf(-(1 / T) * LE));
	                    label_prob[iter_label] = __expf(-(1 / T) * LE);
	                }
	                for (int k = 1; k < 5; k++) {
	                    label_prob[k] = label_prob[k - 1] + label_prob[k];
	                }
	                float rand_num = label_prob[4]*random_numbers[x*IMG_W+y];
	                int label_index = 0;
	                for (int w = 0; w < 4; w++) {
	                	if ((rand_num > label_prob[w]) && (rand_num < label_prob[w + 1])) {
	                		label_index = w + 1;
	                		break;
	                     }
	               }
		 	   	#pragma unroll
	               for(int k=-7;k<8;k++){
					#pragma unroll
	            	   for(int h=-7;h<8;h++){
	                       label_input_img[ (x+k) * IMG_W + y + h ] = labels[label_index];
	                   }
	               }

	              }
	            }
	    }
}

int main(int argc, char* argv[])
{
	if(argc != 2){
		printf("\nPlease input image as an argument\n");
		return 0;
	}

	string img_path1 = argv[1];
    //string img_path2 = argv[2];

	/* See random number generator using random device */
	

	if(!img_path1.empty()) {
		//Load Image from cmd arg
		Mat input_img1 = imread(img_path1, CV_LOAD_IMAGE_GRAYSCALE);
        //Mat input_img2 = imread(img_path2, CV_LOAD_IMAGE_GRAYSCALE);
		//Check if the img is loaded properly
		if(input_img1.empty()) {
			CV_Error(CV_StsBadArg, "Sample image is empty. Please adjust your path, so it points to a valid input image!");
		}
		//Print image dimensions
		IMG_H = input_img1.rows, IMG_W = input_img1.cols;
		printf("Starting.... \nImage Loaded: Height: %d, Width %d.\n", input_img1.rows, input_img1.cols);
		
		unsigned char *input_img1_host = (unsigned char *) (input_img1.data);
		unsigned char *input_img1_device;

		checkCudaErrors(cudaMalloc(&input_img1_device, sizeof(unsigned char) * IMG_W * IMG_H));
		checkCudaErrors(cudaMemcpy(input_img1_device, input_img1_host, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyHostToDevice));

		//Create a new 2D Mat for grey scale and set its pixels to 255
		Mat label_input_img(IMG_W, IMG_H, CV_8UC1);

        label_input_img.setTo(Scalar(255));
        /* start measuring the comutation time */

        unsigned char *label_input_host = (unsigned char *) (label_input_img.data);
        unsigned char *label_input_device;

        checkCudaErrors(cudaMalloc(&label_input_device, sizeof(unsigned char) * IMG_W * IMG_H));
        checkCudaErrors(cudaMemcpy(label_input_device, label_input_host, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyHostToDevice));

        initLabelImg<<<(IMG_H*IMG_W-1)/(PIXEL_PER_THREAD_1 * THREAD_PER_BLOCK)+1, THREAD_PER_BLOCK>>>(IMG_W, IMG_H, label_input_device);

        int update_num = 2 * max_iter;
        dim3 blocks(IMG_W/32, (IMG_H/2-1)/(THREAD_PER_BLOCK/32*PIXEL_PER_THREAD_2)+1, 1);
        dim3 threads(32, THREAD_PER_BLOCK/32, 1);

        float *random_numbers;
        int iteration_per_rand_gen;

       	iteration_per_rand_gen = floor((long long) MAX_MEM_FOR_RAND / (sizeof(float) * IMG_H * IMG_W));
       	checkCudaErrors(cudaMalloc(&random_numbers, iteration_per_rand_gen * sizeof(float) * IMG_H * IMG_W));

//        start timing and begin of max_iter
        tstart = omp_get_wtime();
        
        for (int i = 0; i < update_num; i++) {
        	if ((i % 2 == 0) && ((i / 2) % iteration_per_rand_gen == 0)) {

        				if ((max_iter - i / 2) < iteration_per_rand_gen) {
        					rand_gen_batch(random_numbers,(max_iter - i / 2));

        				}
        				else {
        					rand_gen_batch(random_numbers, iteration_per_rand_gen);
        				}
        	}
        	if (i % 2 == 0) {
        	Black<<<blocks, threads>>>(IMG_W, IMG_H, random_numbers, input_img1_device, label_input_device);
        	}
        	else{
        	White<<<blocks, threads>>>(IMG_W, IMG_H, random_numbers, input_img1_device, label_input_device);
        	}

        	//		end timing and end of max_iter
        	cudaError_t error_code=cudaGetLastError();
        		if (cudaSuccess != error_code) {
        			printf("iteration:%d\n", i);
        			printf("%s\n", cudaGetErrorString(error_code));
        			exit(0);
        		}
        	cudaDeviceSynchronize();
	    }
		tend = omp_get_wtime();
		checkCudaErrors(cudaMemcpy(input_img1_host, input_img1_device, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyDeviceToHost));
		checkCudaErrors(cudaMemcpy(label_input_host, label_input_device, sizeof(unsigned char) * IMG_W * IMG_H, cudaMemcpyDeviceToHost));

		writeToCSV(label_input_host);

		Mat output_img = greyToRGB(label_input_host);

		//free
		checkCudaErrors(cudaFree(random_numbers));
		checkCudaErrors(cudaFree(input_img1_device));
		checkCudaErrors(cudaFree(label_input_device));

		cout << "Time taken for computation: " << tend - tstart << endl;
		cout << "Writing output file..." << endl;
		/* Write the pixel values to a csv file */

        imwrite("./inc/texture_seg.jpg", output_img);
        cout << "Done" << endl;

	}

	return 0;
}
